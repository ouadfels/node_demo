const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...', err));

const authorSchema = new mongoose.Schema({
  name: String,
  bio: String,
  website: String
});

const Author = mongoose.model('Author', authorSchema);

const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  authors: [authorSchema]
}));

async function createCourse(name, authors) {
  const course = new Course({
    name, 
    authors
  }); 
  
  const result = await course.save();
  console.log(result);
}
async function updateAuthor(courseId){
  // const course = await Course.findById(courseId);
  // course.author.name = 'Soheib Ouadfel';
  // course.save();
  //
  // // ou on utilise cette méthode de modification
  // const course2 = await Course.update({ _id: courseId}, {
  //   $set: {
  //     'author.name': 'Ouadfel Soheib'
  //   }
  // });

  // Pour supprimer l'objet author de cours
  const course3 = await Course.update({ _id: courseId}, {
    $unset: {
      'author': ''
    }
  });
}

async function addAuthor(courseId, author) {
  const course = await Course.findById(courseId);
  course.authors.push(author);
  course.save();
}

async function removeAuthor(courseId,authorId) {
  const course = await Course.findById(courseId);
  const author = course.authors.id(authorId);
  author.remove();
  course.save();
}

// updateAuthor('608284bdc6fecf24e8e003c4');
async function listCourses() { 
  const courses = await Course.find();
  console.log(courses);
}

// createCourse('Node Course', [
//   new Author({ name: 'Lotfi' }),
//   new Author({ name: 'Soheib' })
// ]);

// addAuthor('6082892261143d22782d7e3a', new Author({ name: 'Amy'}));
removeAuthor('6082892261143d22782d7e3a', '60828b8379cc322770134062');
