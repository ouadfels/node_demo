const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mongo-exercises')
    .then(() => console.log('Connected to dataBase mongo-exercises  ... '))
    .catch(err => console.error('Could not connect to mongoDB',err))

const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [ String ],
    date: { type: Date, default: Date.now},
    isPublished: Boolean,
    price: Number
});

const Course = mongoose.model('Course', courseSchema);

// Recuprer les cours publié de backend, trier avec leurs noms et selectionné leurs nom et author
async function getCourses(){
    return  await Course
        .find({ tags: 'backend' , isPublished: true})
        .sort({ name: 1})
        .select({ name:1, author: 1});
}

async function run(){
    const courses = await getCourses();
    courses.forEach(element => console.log(element._doc,'***********'));
}

run();



// Recuperer les cours publié de backend et frontend, trier leurs prix decendant et selectionné leurs nom et author

async function getCourses2(){
    return await Course
        .find({isPublished: true})
        .or([ { tags: 'backend'} , { tags: 'frontend'} ])
        .sort({ price: -1})
        .select('name author price');
}

async function run2(){
    const courses = await getCourses2();

    courses.forEach(element => console.log(element._doc,'okkkk 2'));

}

run2();

// Recuperer les cours qui coutent plus que 15 dollars et ils ont by dans leurs titres


async function getCourses3(){
    return await Course
        .find()
        .or([ {price: { $gte:15}} , {name: /.*by.*/i} ])
        .select('name author price');
}

async function run3(){
    const courses = await getCourses3();

    courses.forEach(element => console.log(element._doc));

}

run3();
