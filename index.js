const Joi = require('joi');
const express = require('express');
const logger = require('./logger');
const helmet = require('helmet');
const morgan = require('morgan');
const app = express();

app.use(express.json());
// Ca permet d'envoyer des données dans post format urlencoded
app.use(express.urlencoded({extended: true}));
app.use(express.static('public'));
app.use(helmet());

if(app.get('env') === 'development'){
    app.use(morgan('tiny'));
    console.log('enable morgan');
}

/* Custom MiddleWare
* */
app.use(logger);


const courses = [
    {id: 1, name: 'course1'},
    {id: 2, name: 'course2'},
    {id: 3, name: 'course3'},
    {id: 4, name: 'course4'},
]
app.get('/', function (req, res) {
    res.send('Hello World !!!')
})

app.get('/api/courses',(req, res) => {
    res.send(courses);
});

app.post('/api/courses', (req, res) => {
    const { error } = validateCourse(req.body);
    if (error){
        res.status(400).send(error.details[0].message);
        return;
    }

   const course = {
       id: courses.length + 1,
       name: req.body.name
   }
   courses.push(course);
   res.send(course);
});

app.put('/api/courses/:id',(req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with given ID was not found');
    const { error } = validateCourse(req.body);
    if (error){
        res.status(400).send(error.details[0].message);
        return;
    }
    course.name = req.body.name;
    res.send(course);
});

app.get('/api/courses/:id',(req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with given ID was not found');
    res.send(course);
});

app.delete('/api/courses/:id',(req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return  res.status(404).send('The course with given ID was not found');

    const index = courses.indexOf(course);
    courses.splice(index,1);

    res.send(course);
});

function validateCourse(course){
    const schema = {
        name: Joi.string().min(3).required()
    }

    return Joi.validate(course,schema);
}
app.listen(3000,()=>console.log('server start listning in 3000 ...'))
