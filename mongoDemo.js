const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
    .then(() => console.log('Connected to MongoDB ... '))
    .catch(err => console.error('Could not connect to mongoDB',err))

const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [ String ],
    date: { type: Date, default: Date.now},
    isPublished: Boolean
});

const Course = mongoose.model('Course', courseSchema);

async function createCourse() {
    const course = new Course({
        name: 'Angular Course',
        author: 'Mosh',
        tags: ['angular', 'frontend'],
        isPublished: true
    });

    const result = await course.save();
    console.log(result);
}

 // createCourse();

async function getCourses(){
    // eq (equal)
    // ne (not equal)
    // gt (greater than)
    // gte (greater than or equal)
    // lt (less than)
    // lte (less than or equal)
    // in
    // nin (not in)


    const courses = await Course
        .find({ author: 'Mosh', isPublished: true})
        .limit(10)
        .sort({ name: 1})
        .select({ name:1, tags: 1});
    console.log(courses);

    // Cours qui coute entre 10 et 20 dollars
    const coursesComparingQuery1 = await Course
        .find({ price: { $gte:10 , $lte: 20}})
        .limit(10)
        .sort({ name: 1})
        .select({ name:1, tags: 1});

    // Cours qui coute soit 10, 20 ou 50
    const coursesComparingQuery2 = await Course
        .find({ price: { $in: [10, 20, 50]}})
        .limit(10)
        .sort({ name: 1})
        .select({ name:1, tags: 1});

    // Cours qui leurs author est mosh , ou ils sont publié
    const coursesLogicalQuery2 = await Course
        .find()
        // and
        .or([{ author: 'Mosh'},{ isPublished: true}])
        .limit(10)
        .sort({ name: 1})
        .select({ name:1, tags: 1});


    // Cours qui leurs author doit commencer par mosh
    const coursesLogicalQuery3 = await Course
        // Starts with mosh
        .find({ author: /^Mosh/})

        // Ends with Hamedani
        .find({author: /Hamedani$/i})

        // Contains Mosh
        .find({author: /.*Mosh.*/i})

        .limit(10)
        .sort({ name: 1})
        .select({ name:1, tags: 1});

    const coursesCount = await Course
        .find({ author: 'Mosh', isPublished: true})
        .limit(10)
        .sort({ name: 1})
        .count();
    console.log(coursesCount);

    // Pagination

    const pageNumber = 2;
    const pageSize = 10;

    const coursesPagination = await Course
        .find({ author: 'Mosh', isPublished: true})
        .skip((pageNumber - 1) * pageSize)
        .limit(pageSize)
        .sort({name: 1})
}

// getCourses();


async function updateCourse(id){
    // Approach: Query first
    // findById
    // Modify its properties
    // save

    const course = await Course.findById(id);
    if (!course) return;

    course.set({
        isPublished: true,
        author: 'Another Author'
    });

    // Approach : Update first
    // Update directly
    // Optionnaly : get the updated document
}


